#!/bin/sh
# tadaen sylvermane | jason gibson

# begin script #

# makes sure path is set globally in the /etc/environment file
echo "PATH=\"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games\"" > /etc/environment
echo "http_proxy=\"http://192.168.1.2:3128/\"" >> /etc/environment
# sources for this script as needed
. /etc/environment
# creates non root user for steam
useradd -m -s /bin/bash steam
echo "steam:steam" | chpasswd
# creates the pulseaudio autostart
# makes sure permissions are correct
chown -R steam:steam /home/steam
# launch script creation
if [ ! -f /startup.sh ] ; then
	echo "x11vnc -forever -rfbport 60000 &
startxfce4" > /startup.sh
	chmod 755 /startup.sh
fi
# run the above created launch script as the created user
su -c "while true ; do if ! pgrep xfce4 ; then xvfb-run --server-args=\"-screen 0, 1920x1080x24\" /startup.sh ; fi ; sleep 10 ; done" steam
# finish out and keep running
exec bash

# end script #
