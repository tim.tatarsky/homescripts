#!/bin/sh
# tadaen sylvermane | jason gibson

# functions #

ssh_config_func() {
	echo "Include /etc/ssh/sshd_config.d/*.conf
Port 22223
PasswordAuthentication yes
ChallengeResponseAuthentication no
PermitRootLogin yes
PasswordAuthentication yes
X11Forwarding yes
X11UseLocalhost yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem	sftp	/usr/lib/openssh/sftp-server" > /etc/ssh/sshd_config
}

useradd -m -s /bin/bash virtman
echo "virtman:virtman" | chpasswd
usermod -aG libvirt virtman
chown -R virtman:virtman /home/virtman
ssh_config_func
service ssh start
sleep 10
exec bash

# end script #
