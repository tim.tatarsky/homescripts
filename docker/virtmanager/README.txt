A basic virt-manager that can be run via X forwarding. Good for Windows environment with Linux server?

Build - docker build -t virtman --build-arg http_proxy="${SQUIDPROXY}:3128/" .
Execute - docker run -id --name virtman --network host --mount source=virtman,destination=/home/virtman --restart always virtman

Usage - ssh -X virtman@"$DOCKERSERVER":22223
Password - virtman
Upon prompt enter - virt-manager

I use this with Putty to get myself easy virt-mananger on Windows without having to install 
unneeded packages to my server host. Make sure to create the ssh key as needed.
