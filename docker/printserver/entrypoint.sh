#!/bin/sh
# tadaen sylvermane | jason gibson
# print server entrypoint for docker

CUPSDIR=/etc/cups
CUPSCONFIG="$CUPSDIR"/cupsd.conf
[ -e "$CUPSCONFIG" ] || cp /usr/share/cups/cupsd.conf.default "$CUPSCONFIG"
if ! grep CONFIGDONE "$CUPSCONFIG" ; then
	service cups start
	cupsctl --remote-admin
	sed -i 's/@LOCAL/@LOCAL\n\ \ Allow 192.168.1.*/g' "$CUPSCONFIG"
	sed -i 's/Basic/Basic\nDefaultEncryption IfRequested/' "$CUPSCONFIG"
	sed -i 's/^Listen\ \/run/#Listen\ \/run/g' "$CUPSCONFIG"
	sed -i 's/cups.sock/cups.sock\nListen\ 0.0.0.0:631/' "$CUPSCONFIG"
	echo "CONFIGDONE" >> "$CUPSCONFIG"
	service cups stop
fi
adduser --no-create-home --gecos --system --disabled-password printers
echo printers:printers | chpasswd printers
usermod -aG lpadmin printers
chown -R root:root "$CUPSDIR"
chown root:lp "$CUPSDIR"
for item in cupsd.conf cupsd.conf.0 ppd ssl printers.conf ; do
	chown -R root:lp "$CUPSDIR"/"$item"
done
service cups start
exec bash

