#!/bin/sh
# tadaen sylvermane | jason gibson

# variables #

CRONTAB=/var/spool/cron/crontabs/root

# create crontab #

echo "*/10 * * * *	/etc/init.d/minecraft backup" >> "$CRONTAB"
chmod 600 "$CRONTAB" && chown root:crontab "$CRONTAB"


# make minecraft init file executable #

chmod 755 /etc/init.d/minecraft

# start services #

service cron start
service minecraft start

# tail off #

exec bash
