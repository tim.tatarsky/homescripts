#!/bin/sh
# tadaen sylvermane | jason gibson
# source for squid ssl docker container init script

# variables #

# directory to put certificates #

CERTSDIR=/etc/squid/certs
SQUIDDATA=/etc/squid/squiddata

# openssl keygen variables - customize these #

COUNTRY="US"
STATE="AZ"
CITY="TUCSON"
ORGANIZATION="ORG"
ORGANIZATIONUNIT="LAN"
COMMONNAME="NAME"
EMAIL="EMAIL@ADDRESS"
DAYS="365"

# begin script #

if [ ! "$(ls -A ${CERTSDIR})" ] ; then
	mkdir -p "$CERTSDIR"
	openssl req \
		-new \
		-newkey rsa:2048 \
		-sha256 \
		-days "$DAYS" \
		-nodes \
		-x509 \
		-extensions v3_ca \
		-keyout "$CERTSDIR"/myCA.pem \
		-out "$CERTSDIR"/myCA.pem \
		-subj "/C=${COUNTRY}/" \
		-subj "/ST=${STATE}/" \
		-subj "/L=${CITY}/" \
		-subj "/O=${ORGANIZATION}/" \
		-subj "/OU=${ORGANIZATIONUNIT}/" \
		-subj "/CN=${COMMONNAME}/" \
		-subj "/emailAddress=${EMAIL}"
	openssl x509 -outform DER -in "$CERTSDIR"/myCA.pem \
	-out "$CERTSDIR"/SQUID-CA-FOR-IMPORT.der
	chmod 700 "$CERTSDIR" && chmod 744 "$CERTSDIR"/*.pem
	[ -d "$SQUIDDATA" ] || mkdir -p "$SQUIDDATA"
	chown -R proxy:proxy "$SQUIDDATA"
	/usr/lib/squid/security_file_certgen -c -s "$SQUIDDATA"/ssl_db -M 20MB
fi

chown -R proxy:proxy "$SQUIDDATA"
chown -R proxy:proxy "$CERTSDIR"
[ -e /var/run/squid.pid ] && rm /var/run/squid.pid
sed -i 's/\/var\/spool\/squid/\/etc\/squid\/squiddata/g' /etc/squid/squid.conf
service squid start
sleep 10
pgrep squid || service squid start
exec bash

# end script #
