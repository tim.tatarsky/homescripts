#!/bin/sh
# tadaen sylvermane | jason gibson
# modified from graysky2 kodi xscreensaver deactivate
# https://github.com/graysky2/kodi-prevent-xscreensaver

# begin script #

# exit if command not exist

which xscreensaver-command || exit 1

# if "$1" running then heartbeat xscreensaver to prevent activation

while true ; do
		pidof "$1" && \
				xscreensaver-command -deactivate &>/dev/null
		sleep 49
done

# end script #
