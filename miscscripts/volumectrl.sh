#!/bin/sh
# tadaen sylvermane
# basic volume control for window manager use. direct control of pulse via 
# pactl. global volume and mute controls

# variable to pick default sink. get from unique identifier in 
# pactl list sinks short.

DEFAULTSINK=pci-0000_00_1b.0

# begin script #

# sink select. currently mapping to openbox C-XF86Audio[Lower|Raise|Mute]

case "$1" in
	hdmi|corsair|pci-0000_00_1b.0)
		SINKID=$(pactl list sinks short | grep -i "$1" | awk '{print $1}')
		pactl set-default-sink "$SINKID"
		;;
esac

# volume and mute. currently mapping to openbox XF86Audio[Lower|Raise|Mute]. 
# volume is in 10% increments. startup sets hdmi as default, mute turned off, 
# and volume 20%

pactl list sinks short | while read line ; do
	CURRENTSINK=$(echo "$line" | awk '{print $1}')
	echo "$CURRENTSINK"
	case "$1" in
		+|-)
			pactl set-sink-volume "$CURRENTSINK" "$1"10%
			;;
		mute)
			pactl set-sink-mute "$CURRENTSINK" toggle
			;;
		*)
			pactl set-sink-volume "$CURRENTSINK" 20%
			pactl set-sink-mute "$CURRENTSINK" 0
			if echo "$line" | grep -i "$DEFAULTSINK" ; then
				pactl set-default-sink "$CURRENTSINK"
			fi
			;;
	esac
done

# end script #
