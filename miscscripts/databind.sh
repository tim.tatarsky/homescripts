#!/bin/sh
# non volatile directories for user data

DATAMNT=/mnt/userdata

DIRS=".gnupg
      .ssh
      .local/share/flatpak
      .var
      Downloads
      Documents
      Music
      Pictures
      Videos"

for user in /home/* ; do
  USERNAME=$(basename "$user")

  # skip non user directories
  if [ "$USERNAME" = partimg ] || \
     [ "$USERNAME" = lost+found ] ; then
    continue
  fi
  
  USERDIR=/home/"$USERNAME"

  case "$1" in
    boot)
      # loop through specified directories to create directory 
      # in both places as needed then mount and set owner
      for dir in $DIRS ; do
        DIRNAME="$USERDIR"/"$dir"
        DATADIR="$DATAMNT"/"$USERNAME"
        [ -d "$DIRNAME" ] || \
          mkdir -p "$DIRNAME"
        [ -d "$DATADIR"/"$dir" ] || \
          mkdir -p "$DATADIR"/"$dir"
        mountpoint "$DIRNAME" || \
          mount -o bind "$DATADIR"/"$dir" "$DIRNAME"
        chown -R "$USERNAME":"$USERNAME" "$USERDIR"
      done
      
      # symlinks for flatpak evolution integrate with shell
      EVOLDIR="$USERDIR"/.var/app/org.gnome.Evolution

      [ -L "$USERDIR"/.config/evolution ] || \
      	ln -sf "$EVOLDIR"/config/evolution \
      	"$USERDIR"/.config/evolution
      		
      for i in evolution evolution.bin ; do
      	[ -L "$USERDIR"/.cache/"$i" ] || \
      		ln -sf "$EVOLDIR"/cache/"$i" \
      		"$USERDIR"/.cache/"$i"
      done
      ;;
    shutdown)
      for dir in $DIRS ; do
        umount "$USERDIR"/"$dir"
      done
      ;;
  esac
done
