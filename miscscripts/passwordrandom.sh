#!/bin/sh
# tadaen sylvermane | jason gibson
# random password list generator

# variables #

PWGENS=/tmp/pwgen1

# begin script #

for i in $(seq 1 50) ; do
	for j in $(seq 1 5) ; do
		pwgen --symbols --numerals >> "$PWGENS"
	done
	tr -d '\n' < "$PWGENS" >> "$PWGENS"
	echo "${i} $(tail -n 1 ${PWGENS})" >> "$1"
	rm "$PWGENS"
done

# end script #
