#!/bin/sh

if which gprof > /dev/null ; then
    if [ -f "$1" ] ; then
        DIRPATH=$(dirname "$1")
        if ! gprof "$1" "$DIRPATH"/gmon.out > "$DIRPATH"/profiletest.txt ; then
            echo "you need to recompile and link your program"
            echo "with the -pg flag"
        fi
    else
        echo "file not found"
        echo "please supply path to executable to test"
    fi
else
    echo "gprof should be in binutils but is not found"
    echo "something is amiss"
fi
