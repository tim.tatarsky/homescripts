#!/bin/sh

PATH="/usr/bin"
LOGFILE=log.txt
KEEPLOG=no
VGRINDOPTS="-s --leak-check=full --track-origins=yes"

test_error_leak() {
  if [ $(grep 'no leaks are possible' "$2" | wc  -l) != "$1" ] ; then
    echo "leaks found at test ${1}"
    KEEPLOG=yes
    exit
  fi

  if [ $(grep '0 errors from 0 contexts' "$2" | wc -l) != "$1" ] ; then
    echo "errors found at test ${1}"
    KEEPLOG=yes
    exit
  fi
}

# if improper variables

if [ ! "$1" ] || [ ! "$2" ] ; then
  echo "args - (number of loops | executable to test)"
  exit
fi

# https://stackoverflow.com/a/3951175

case "$1" in
  ''|*[!0-9]*)
    echo "not a valid number of loops"
    exit
esac

# if executable not found then bail out

if [ ! -e "$2" ] ; then
  echo "file not found!"
  echo "check path and try again."
  exit
fi

# remove old logfile

[ -e "$LOGFILE" ] && rm "$LOGFILE"

clear

# loop n number of times. have had it fail on random runs
# best to run multiple test loops.

for i in $(seq 1 "$1") ; do
  echo ""
  echo "test ${i} working now. standby!"
  echo "test ${i} starting" >> "$LOGFILE"
  valgrind \
    $VGRINDOPTS \
    "$2" 2>&1 | tee -a "$LOGFILE" > /dev/null
  test_error_leak "$i" "$LOGFILE"
done

# remove logfile if no problems

if [ "$KEEPLOG" = no ] ; then
  clear
  echo ""
  echo "valgrind passed ${1} tests cleanly"
  echo "options used : ${VGRINDOPTS}"
  echo ""
  rm "$LOGFILE"
fi

