#!/bin/sh
# tadaen sylvermane | jason gibson
# simple home router setup

# sources #

# https://gist.github.com/maprangzth/453373f3052a0bd7d77b8689ada4dc40
# https://www.systutorials.com/setting-up-gateway-using-iptables-and-route-on-linux/
# https://arstechnica.com/gadgets/2016/04/the-ars-guide-to-building-a-linux-router-from-scratch/

ROUTERVARS=/tmp/routerif.tmp
GOODGW=10.88.88.1
MEDIOCREGW=10.254.197.1
GOODGWTABLE=bluespan
WIFEWORKGW=192.168.1.250


# functions #

interface_loop_func() {
	for interface in $(find /sys/class/net/ -maxdepth 1) ; do
		ifname=$(basename "$interface")
		ifip=$(ip addr | grep 'inet ' | grep "$ifname" | awk '{print $2}' \
		| cut -d\/ -f 1 | head -n 1)
		case "$ifname" in
			net|lo|virbr*|vnet*|veth*|docker*)
				continue
				;;
			*)
				"$@"
				;;
		esac
	done
}

interface_start_func() {
	if echo "$ifip" | grep -q "$1" ; then
		router_config_func "$1"
	else
		break
	fi
}

interface_iptables_func() {
	if [ ! -z "$ifip" ] ; then
		case "$ifip" in
			"$1".*)
				# allow dns server on this host
				iptables -A INPUT -i "$ifname" -p tcp --dport 53 -j ACCEPT
				iptables -A INPUT -i "$ifname" -p udp --dport 53 -j ACCEPT
				# allow ssh on this host
				iptables -A INPUT -i "$ifname" -p tcp --dport 22 -j ACCEPT
				# allow dhcp server on this host
				iptables -A INPUT -i "$ifname" -p udp --dport 67 -j ACCEPT
				# allow squid proxy server on this host
				iptables -A INPUT -i "$ifname" -p tcp --dport 3128 -j ACCEPT
				iptables -A INPUT -i "$ifname" -p tcp --dport 3130 -j ACCEPT
				echo "${ifname} lan" >> "$ROUTERVARS"
				;;
			*)
				# configure wan address to nat
				iptables -t nat -A POSTROUTING -o "$ifname" -j MASQUERADE
#				https://www.reddit.com/r/linuxadmin/comments/aqx7sl/iptables_masquerade_appears_to_be_leaking/
				iptables -A FORWARD -o "$ifname" -m state --state INVALID -j DROP
				echo "${ifname} wan" >> "$ROUTERVARS"
				;;
		esac
	fi
}

router_config_func() {
	if [ ! -f "$ROUTERVARS" ] ; then
		# squid rules - comment if needed
		iptables -A PREROUTING -t nat -p tcp --dport 80 -j REDIRECT --to-ports 3130
		# end squid prerouting
		# specific gateway application prerouting
		# wife work router
		iptables -A PREROUTING -t mangle -s "$WIFEWORKGW" -j MARK --set-mark 100
		# zoom
		iptables -A PREROUTING -t mangle -p tcp --dport 8801 -j MARK --set-mark 100
		iptables -A PREROUTING -t mangle -p tcp --dport 8802 -j MARK --set-mark 100
		for port in 8801 8802 8803 8804 8805 8806 8807 8808 8809 8810 ; do
			iptables -A PREROUTING -t mangle -p udp --dport "$port" -j MARK --set-mark 100
		done
		# world of warcraft ports
		for port in 3724 1119 6012 ; do
			for type in tcp udp ; do
				iptables -A PREROUTING -t mangle -p "$type" --dport "$port" -j MARK --set-mark 100
			done
		done
#		https://arstechnica.com/gadgets/2016/04/the-ars-guide-to-building-a-linux-router-from-scratch/
		iptables -A INPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -i lo -j ACCEPT
		iptables -A INPUT -p icmp -j ACCEPT
		iptables -A INPUT -m state --state ESTABLISHED -j ACCEPT
		iptables -A INPUT -p udp -m udp --dport 33434:33523 -j REJECT \
		--reject-with icmp-port-unreachable
		#
		interface_loop_func interface_iptables_func "$1"
		iptables -A INPUT -j DROP
		iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
		# forward lan interface to wan(s)
		for lan_if in $(grep lan "$ROUTERVARS" | awk '{print $1}') ; do
			for wan_if in $(grep wan "$ROUTERVARS" | awk '{print $1}') ; do
				iptables -A FORWARD -i "$lan_if" -o "$wan_if" -j ACCEPT
			done
		done
		# drop anything that doesn't fit previous forward rules
		iptables -A FORWARD -j DROP
		rm "$ROUTERVARS"
	fi
}

case "$1" in
	load)
		# allow nat forwarding
		sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf
		sysctl -p
		# until lan ip active
		until ip addr | grep -e '^\ *inet\ ' | awk '{print $2}' | grep -v 127.0.0 \
		| grep -v "$2" ; do
		        sleep 5
		done
		interface_loop_func interface_start_func "$2"
		# this stuff is for dual wan
#		ip route add "$WIFEWORKGW" via "$GOODGW"
#		ip rule add from all fwmark 100 table "$GOODGWTABLE"
#		ip route delete default
#		ip route add default via "$GOODGW" table "$GOODGWTABLE"
#		ip route add default via "$MEDIOCREGW"
#		https://www.linuxquestions.org/questions/linux-networking-3/dual-wan-ip-tables-one-wan-is-abysmally-slow-4175695439/
#		fresh start on load
#		conntrack -F
		;;
	stop)
		iptables -P INPUT ACCEPT
		iptables -P FORWARD ACCEPT
		iptables -P OUTPUT ACCEPT
		iptables -t nat -F
		iptables -t mangle -F
		iptables -F
		iptables -X
		;;
	*)
		echo "usage: ${0} (load (lan subnet ex. 192.168.0)| restart)"
		exit 0
		;;
esac

# end script #
